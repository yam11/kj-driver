import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFF013237);
  static const Color seconedaryColor = Color(0xFFFE979C);

  static const Color thirdColor = Color(0xFFFEAE96);
  static const Color fourthColor = Color(0xFFF6E8DF);
  static const Color grayColor = Color(0xFF707070);
  static const Color whiteColor = Color(0xFFFFFFFF);
}
