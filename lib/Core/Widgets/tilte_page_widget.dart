import 'package:flutter/material.dart';
import 'package:kjdriver/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class TitlePageWidget extends StatelessWidget {
  String title;
  TitlePageWidget({required this.title, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.w, vertical: 1.h),
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: AppColors.primaryColor.withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.w),
          border: Border.all(width: 2, color: AppColors.primaryColor)),
      child: Text(
        title,
        style: TextStyle(
            color: AppColors.seconedaryColor,
            fontSize: 18.sp,
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
