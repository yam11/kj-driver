import 'package:flutter/material.dart';
import 'package:kjdriver/Core/Constants/app_assets.dart';
import 'package:sizer/sizer.dart';

class BackGraoundWidget extends StatelessWidget {
  Widget child;
  BackGraoundWidget({required this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppAssets.homeBackGraound), fit: BoxFit.fill)),
      child: child,
    );
  }
}
