import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kjdriver/App/app_localizations.dart';
import 'package:kjdriver/Core/Constants/app_assets.dart';
import 'package:kjdriver/Core/Constants/app_colors.dart';
import 'package:kjdriver/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:kjdriver/Feature/Auth/Presentation/Pages/log_in_page.dart';
import 'package:kjdriver/Feature/Main/Presentation/Pages/main_pages.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool animate = false;
  @override
  void initState() {
    startAnimation();

    Timer(const Duration(seconds: 4), () {
      if (!AppSharedPreferences.hasToken) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                child: LogInPage(),
                type: PageTransitionType.rightToLeft,
                duration: const Duration(milliseconds: 300)));
      } else {
        Navigator.pushReplacement(
            context,
            PageTransition(
                child: MainPage(),
                type: PageTransitionType.rightToLeft,
                duration: const Duration(milliseconds: 300)));
      }
    });
    super.initState();
  }

  Future startAnimation() async {
    await Future.delayed(const Duration(milliseconds: 500));
    setState(() {
      animate = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            image:
                DecorationImage(image: AssetImage(AppAssets.homeBackGraound))),
        child: Stack(
          children: [
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                top: animate ? 5.h : -5.h,
                left: animate ? 5.w : -5.w,
                child: Container(
                  height: 10.h,
                  width: 18.w,
                  decoration: const BoxDecoration(
                      image:
                          DecorationImage(image: AssetImage(AppAssets.blue))),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                top: animate ? 5.h : -5.h,
                right: animate ? 5.w : -5.w,
                child: Container(
                  height: 10.h,
                  width: 18.w,
                  decoration: const BoxDecoration(
                      image:
                          DecorationImage(image: AssetImage(AppAssets.yallow))),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                top: 25.h,
                left: !AppSharedPreferences.hasArLang
                    ? animate
                        ? 10.w
                        : -10.w
                    : null,
                right: AppSharedPreferences.hasArLang
                    ? animate
                        ? 10.w
                        : -10.w
                    : null,
                child: AnimatedOpacity(
                  duration: const Duration(milliseconds: 1600),
                  opacity: animate ? 1 : 0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome To".tr(context),
                        style: TextStyle(
                            color: AppColors.seconedaryColor,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          Text(
                            "App Driver".tr(context),
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: 30.sp,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                        ],
                      ),
                    ],
                  ),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                bottom: animate ? 20.h : 10.h,
                child: AnimatedOpacity(
                  duration: const Duration(milliseconds: 1600),
                  opacity: animate ? 1 : 0,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10.w),
                    height: 40.h,
                    width: 70.w,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(AppAssets.schoolBusIcon1),
                            fit: BoxFit.fill)),
                  ),
                )),
            // AnimatedPositioned(
            //     duration: const Duration(milliseconds: 1600),
            //     bottom: animate ? 30.h : 10.h,
            //     right: 8.w,
            //     child: AnimatedOpacity(
            //       duration: const Duration(milliseconds: 1600),
            //       opacity: animate ? 1 : 0,
            //       child: Container(
            //         height: 30.h,
            //         width: 40.w,
            //         alignment: Alignment.center,
            //         decoration: const BoxDecoration(
            //             image: DecorationImage(
            //                 image: AssetImage(AppAssets.schoolBusIcon2),
            //                 fit: BoxFit.fill)),
            //       ),
            //     )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                right: animate ? 5.w : -5.w,
                bottom: animate ? 5.h : -10.h,
                child: Container(
                  height: 10.h,
                  width: 18.w,
                  decoration: const BoxDecoration(
                      image:
                          DecorationImage(image: AssetImage(AppAssets.purple))),
                )),
          ],
        ),
      ),
    );
  }
}
