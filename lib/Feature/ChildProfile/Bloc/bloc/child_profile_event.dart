part of 'child_profile_bloc.dart';

@immutable
abstract class ChildProfileEvent {}

class ChildProfile extends ChildProfileEvent {
  int childId;
  ChildProfile({required this.childId});
}
