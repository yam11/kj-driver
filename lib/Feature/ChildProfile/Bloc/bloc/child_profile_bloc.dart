import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjdriver/Core/Api/ExceptionsHandle.dart';
import 'package:kjdriver/Core/Api/Network.dart';
import 'package:kjdriver/Core/Api/Urls.dart';
import 'package:kjdriver/Feature/ChildProfile/Models/child_profile_model.dart';
import 'package:meta/meta.dart';

part 'child_profile_event.dart';
part 'child_profile_state.dart';

class ChildProfileBloc extends Bloc<ChildProfileEvent, ChildProfileState> {
  ChildProfileModel? childProfileModel;
  ChildProfileBloc() : super(ChildProfileInitial()) {
    on<ChildProfileEvent>((event, emit) async {
      if (event is ChildProfile) {
        emit(LoadingToGetChildProfile());
        try {
          final response = await Network.getData(
              url: "${Urls.childDetails}${event.childId}");
          childProfileModel = ChildProfileModel.fromJson(response.data);
          emit(SuccessToGetChildProfile());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildProfile(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildProfile(message: "error"));
          }
        }
      }
    });
  }
}
