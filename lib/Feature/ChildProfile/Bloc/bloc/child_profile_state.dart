part of 'child_profile_bloc.dart';

@immutable
abstract class ChildProfileState {}

class ChildProfileInitial extends ChildProfileState {}

class SuccessToGetChildProfile extends ChildProfileState {}

class ErrorToGetChildProfile extends ChildProfileState {
  final String message;
  ErrorToGetChildProfile({required this.message});
}

class LoadingToGetChildProfile extends ChildProfileState {}
