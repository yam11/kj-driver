class ChildProfileModel {
  Data? data;

  ChildProfileModel({this.data});

  ChildProfileModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? firstName;
  int? guaridanId;
  int? classroomId;
  int? busLineId;
  String? status;
  String? birthDate;
  Guardian? guardian;
  Classroom? classroom;
  String ? image;
  // List<Null>? orders;
  // List<Null>? reports;

  Data(
      {this.id,
      this.firstName,
      this.guaridanId,
      this.classroomId,
      this.busLineId,
      this.status,
      this.birthDate,
      this.guardian,
      this.classroom,
      this.image,
      // this.orders,
      // this.reports
      
      });

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    guaridanId = json['guaridanId'];
    classroomId = json['classroomId'];
    busLineId = json['busLineId'];
    status = json['status'];
    birthDate = json['birthDate'];
    guardian = json['guardian'] != null
        ? new Guardian.fromJson(json['guardian'])
        : null;
    classroom = json['classroom'] != null
        ? new Classroom.fromJson(json['classroom'])
        : null;
    image = json['image'];
    // if (json['orders'] != null) {
    //   orders = <Null>[];
    //   json['orders'].forEach((v) {
    //     orders!.add(new Null.fromJson(v));
    //   });
    // }
    // if (json['reports'] != null) {
    //   reports = <Null>[];
    //   json['reports'].forEach((v) {
    //     reports!.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['guaridanId'] = this.guaridanId;
    data['classroomId'] = this.classroomId;
    data['busLineId'] = this.busLineId;
    data['status'] = this.status;
    data['birthDate'] = this.birthDate;
    if (this.guardian != null) {
      data['guardian'] = this.guardian!.toJson();
    }
    if (this.classroom != null) {
      data['classroom'] = this.classroom!.toJson();
    }
    data['image'] = this.image;
    // if (this.orders != null) {
    //   data['orders'] = this.orders!.map((v) => v.toJson()).toList();
    // }
    // if (this.reports != null) {
    //   data['reports'] = this.reports!.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class Guardian {
  int? id;
  String? firstName;
  String? lastName;
  String? address;
  String? phoneNumber;
  int? balance;

  Guardian(
      {this.id,
      this.firstName,
      this.lastName,
      this.address,
      this.phoneNumber,
      this.balance});

  Guardian.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['LastName'];
    address = json['address'];
    phoneNumber = json['phoneNumber'];
    balance = json['balance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['address'] = this.address;
    data['phoneNumber'] = this.phoneNumber;
    data['balance'] = this.balance;
    return data;
  }
}

class Classroom {
  int? id;
  int? teacherId;
  String? level;

  Classroom({this.id, this.teacherId, this.level});

  Classroom.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    teacherId = json['teacherId'];
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['teacherId'] = this.teacherId;
    data['level'] = this.level;
    return data;
  }
}
