import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kjdriver/App/app_localizations.dart';
import 'package:kjdriver/Core/Constants/app_assets.dart';
import 'package:kjdriver/Core/Constants/app_colors.dart';
import 'package:kjdriver/Core/Widgets/app_bar_widget.dart';
import 'package:kjdriver/Core/Widgets/back_graound_widget.dart';
import 'package:kjdriver/Core/Widgets/error_message_widget.dart';
import 'package:kjdriver/Core/Widgets/loading_widget.dart';
import 'package:kjdriver/Core/Widgets/title_widget.dart';
import 'package:kjdriver/Feature/ChildProfile/Bloc/bloc/child_profile_bloc.dart';
import 'package:kjdriver/Feature/ChildProfile/Presentation/Widgets/child_profile_widget.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';

import 'package:map_launcher/map_launcher.dart' as maplauncher;

class ChildProFilePage extends StatefulWidget {
  String lat;
  String lang;
  int childId;
  ChildProFilePage(
      {required this.childId, required this.lang, required this.lat, Key? key})
      : super(key: key);

  @override
  State<ChildProFilePage> createState() => _ChildProFilePageState();
}

class _ChildProFilePageState extends State<ChildProFilePage> {
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> myMarker = {};
  late CameraPosition initialSettings;

  @override
  void initState() {
    initialSettings = CameraPosition(
      target: LatLng(double.parse(widget.lat), double.parse(widget.lang)),
      zoom: 16.0,
    );
    super.initState();
  }

  ChildProfileBloc childProfileBloc = ChildProfileBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
        child: BlocProvider(
          create: (context) =>
              childProfileBloc..add(ChildProfile(childId: widget.childId)),
          child: SafeArea(
            child: BlocConsumer<ChildProfileBloc, ChildProfileState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is ErrorToGetChildProfile) {
                  return ErrorMessageWidget(
                      onPressed: () {}, message: state.message);
                }
                if (state is SuccessToGetChildProfile) {
                  return SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10.h,
                        ),
                        CircleAvatar(
                          radius: 10.w,
                          backgroundColor: AppColors.whiteColor,
                          backgroundImage:
                              const AssetImage(AppAssets.myProfileIcon),
                        ),
                        SizedBox(
                          height: 4.h,
                        ),
                        ChildProFileWidget(
                          name: childProfileBloc
                              .childProfileModel!.data!.firstName!,
                          motherName: childProfileBloc.childProfileModel!.data!
                                  .guardian!.firstName! +
                              childProfileBloc
                                  .childProfileModel!.data!.guardian!.lastName!,
                          address: childProfileBloc
                              .childProfileModel!.data!.guardian!.address!,
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        TitleWidget(title: "Address on the map".tr(context)),
                        Container(
                          height: 30.h,
                          width: double.infinity,
                          margin: EdgeInsets.symmetric(
                              horizontal: 2.w, vertical: 2.h),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  width: 2, color: AppColors.grayColor)),
                          child: StatefulBuilder(builder:
                              (BuildContext context, StateSetter setState) {
                            return GoogleMap(
                              gestureRecognizers: <
                                  Factory<OneSequenceGestureRecognizer>>{
                                Factory<OneSequenceGestureRecognizer>(
                                  () => EagerGestureRecognizer(),
                                ),
                              },
                              mapType: MapType.hybrid,
                              initialCameraPosition: initialSettings,
                              myLocationEnabled: true,
                              onMapCreated: (GoogleMapController controller) {
                                setState(() {
                                  myMarker.add(Marker(
                                      consumeTapEvents: true,
                                      onTap: () async {
                                        try {
                                          if ((await maplauncher.MapLauncher
                                              .isMapAvailable(maplauncher
                                                  .MapType.google))!) {
                                            await maplauncher.MapLauncher
                                                .showMarker(
                                              mapType:
                                                  maplauncher.MapType.google,
                                              coords: maplauncher.Coords(
                                                  double.parse(widget.lat),
                                                  double.parse(widget.lang)),
                                              title: 'trip location',
                                            );
                                          }
                                          print('hello world map 2');
                                        } catch (error) {
                                          print(
                                              'hello world map 2 error $error');
                                        }
                                      },
                                      markerId: MarkerId("1"),
                                      position: LatLng(double.parse(widget.lat),
                                          double.parse(widget.lang))));
                                });
                                _controller.complete(controller);
                              },
                              markers: myMarker,
                            );
                          }),
                        )
                      ],
                    ),
                  );
                } else {
                  return const LoadingWidget();
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
