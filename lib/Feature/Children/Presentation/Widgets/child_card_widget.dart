import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjdriver/App/app_localizations.dart';
import 'package:kjdriver/Core/Constants/app_assets.dart';
import 'package:kjdriver/Core/Constants/app_colors.dart';
import 'package:kjdriver/Feature/ChildProfile/Presentation/Pages/child_profile_page.dart';
import 'package:kjdriver/Feature/Children/Bloc/bloc/change_state_bloc.dart';
import 'package:kjdriver/Feature/Main/Bloc/bloc/children_bus_line_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class ChildCardWidget extends StatelessWidget {
  int childId;
  String name;
  String image;
  String childState;
  int index;
  ChildrenBusLineBloc childrenBusLineBloc;

  ChildCardWidget(
      {required this.image,
      required this.childId,
      required this.childrenBusLineBloc,
      required this.index,
      required this.name,
      required this.childState,
      Key? key})
      : super(key: key);

  ChangeStateBloc changeStateBloc = ChangeStateBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => changeStateBloc,
      child: BlocConsumer<ChangeStateBloc, ChangeStateState>(
        listener: (context, state) {
          if (state is SuccessToChangeState) {
            if (childState == "H") {
              childState = "B";
            } else {
              childState = "H";
            }
          }
        },
        builder: (context, state) {
          return Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5.w,
              vertical: 3.h,
            ),
            padding: EdgeInsets.symmetric(
              horizontal: 2.w,
            ),
            decoration: BoxDecoration(color: AppColors.whiteColor, boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: 2,
                  blurRadius: 4,
                  offset: const Offset(3, 3)),
            ]),
            child: Column(
              children: [
                SizedBox(
                  height: 3.h,
                ),
                Row(
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage(image),
                      radius: 8.w,
                    ),
                    SizedBox(
                      width: 1.w,
                    ),
                    Expanded(
                        child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Name".tr(context),
                              style: TextStyle(
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.primaryColor),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Expanded(
                              child: Text(
                                name,
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: AppColors.seconedaryColor),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "State".tr(context),
                              style: TextStyle(
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.primaryColor),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Expanded(
                              child: Row(
                                children: [
                                  Text(
                                    childState,
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        color: AppColors.seconedaryColor,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  
                                  childState == "B"
                                      ? Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5.w),
                                          height: 2.h,
                                          width: 3.w,
                                          decoration: const BoxDecoration(
                                              color: Colors.green,
                                              shape: BoxShape.circle),
                                        )
                                      : Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5.w),
                                          height: 2.h,
                                          width: 3.w,
                                          decoration: const BoxDecoration(
                                              color: Colors.red,
                                              shape: BoxShape.circle),
                                        )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    )),
                    MaterialButton(
                      onPressed: () {
                        if (childState == "B") {
                          changeStateBloc
                              .add(ChangeState(childId: childId, state: "H"));
                        } else {
                          changeStateBloc
                              .add(ChangeState(childId: childId, state: "B"));
                        }
                      },
                      color: AppColors.primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.w)),
                      child: state is! LoadingToChangeState
                          ? Text(
                              "State",
                              style: TextStyle(
                                  color: AppColors.seconedaryColor,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.bold),
                            )
                          : SizedBox(
                              height: 1.h,
                              width: 2.w,
                              child: const CircularProgressIndicator(
                                color: AppColors.seconedaryColor,
                              ),
                            ),
                    )
                  ],
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        PageTransition(
                            child: ChildProFilePage(
                              lat: " 33.511749",
                              lang: "36.310276",
                              childId: childrenBusLineBloc.childrenBusLineModel!
                                  .data!.children![index].id!,
                            ),
                            type: PageTransitionType.rightToLeft,
                            duration: const Duration(milliseconds: 300)));
                  },
                  minWidth: double.infinity,
                  color: AppColors.primaryColor,
                  child: Text(
                    "View Profile".tr(context),
                    style: TextStyle(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                        color: AppColors.seconedaryColor),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
