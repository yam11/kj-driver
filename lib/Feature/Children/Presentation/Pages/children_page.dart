import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:kjdriver/Core/Constants/app_assets.dart';
import 'package:kjdriver/Feature/ChildProfile/Presentation/Pages/child_profile_page.dart';
import 'package:kjdriver/Feature/Children/Presentation/Widgets/child_card_widget.dart';
import 'package:kjdriver/Feature/Main/Bloc/bloc/children_bus_line_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class ChildrenPage extends StatelessWidget {
  ChildrenBusLineBloc childrenBusLineBloc;
  ChildrenPage({required this.childrenBusLineBloc, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: AnimationLimiter(
      child: ListView.builder(
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        itemCount:
            childrenBusLineBloc.childrenBusLineModel!.data!.children!.length,
        itemBuilder: (context, index) {
          return AnimationConfiguration.staggeredList(
            position: index,
            delay: const Duration(milliseconds: 100),
            child: SlideAnimation(
              duration: const Duration(milliseconds: 2500),
              curve: Curves.fastLinearToSlowEaseIn,
              horizontalOffset: 30,
              verticalOffset: 350,
              child: FlipAnimation(
                duration: const Duration(milliseconds: 3000),
                curve: Curves.fastLinearToSlowEaseIn,
                flipAxis: FlipAxis.y,
                child: ChildCardWidget(
                  childId: childrenBusLineBloc
                      .childrenBusLineModel!.data!.children![index].id!,
                  index: index,
                  name: childrenBusLineBloc
                      .childrenBusLineModel!.data!.children![index].firstName!,
                  childState: childrenBusLineBloc
                      .childrenBusLineModel!.data!.children![index].status!,
                  childrenBusLineBloc: childrenBusLineBloc,
                  image: AppAssets.myProfileIcon,
                ),
              ),
            ),
          );
        },
      ),
    ));
  }
}
