part of 'change_state_bloc.dart';

@immutable
abstract class ChangeStateEvent {}

class ChangeState extends ChangeStateEvent {
  int childId;
  String state;
  ChangeState({required this.state, required this.childId});
}
