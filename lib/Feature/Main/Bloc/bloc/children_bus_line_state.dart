part of 'children_bus_line_bloc.dart';

@immutable
abstract class ChildrenBusLineState {}

class ChildrenBusLineInitial extends ChildrenBusLineState {}

class SucccesToGetChildrenBusLine extends ChildrenBusLineState {}

class ErrorToGetChildrenBusLine extends ChildrenBusLineState {
  final String message;
  ErrorToGetChildrenBusLine({required this.message});
}

class LoadingToGetChildrenBusLine extends ChildrenBusLineState {}
