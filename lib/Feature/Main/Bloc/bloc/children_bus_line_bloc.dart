import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjdriver/Core/Api/ExceptionsHandle.dart';
import 'package:kjdriver/Core/Api/Network.dart';
import 'package:kjdriver/Core/Api/Urls.dart';
import 'package:kjdriver/Feature/Main/Models/children_bus_line_model.dart';
import 'package:meta/meta.dart';

part 'children_bus_line_event.dart';
part 'children_bus_line_state.dart';

class ChildrenBusLineBloc
    extends Bloc<ChildrenBusLineEvent, ChildrenBusLineState> {
  ChildrenBusLineModel? childrenBusLineModel;
  ChildrenBusLineBloc() : super(ChildrenBusLineInitial()) {
    on<ChildrenBusLineEvent>((event, emit) async {
      if (event is ChildrenBusLine) {
        emit(LoadingToGetChildrenBusLine());
        try {
          final response = await Network.getData(url: "${Urls.busLines}22");
          childrenBusLineModel = ChildrenBusLineModel.fromJson(response.data);
          emit(SucccesToGetChildrenBusLine());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildrenBusLine(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildrenBusLine(message: "error"));
          }
        }
      }
    });
  }
}
