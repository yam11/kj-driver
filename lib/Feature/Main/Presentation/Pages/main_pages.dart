import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjdriver/App/app_localizations.dart';
import 'package:kjdriver/Core/Constants/app_colors.dart';
import 'package:kjdriver/Core/Widgets/back_graound_widget.dart';
import 'package:kjdriver/Core/Widgets/error_message_widget.dart';
import 'package:kjdriver/Core/Widgets/loading_widget.dart';
import 'package:kjdriver/Feature/Children/Presentation/Pages/children_page.dart';
import 'package:kjdriver/Feature/Main/Bloc/bloc/children_bus_line_bloc.dart';
import 'package:kjdriver/Feature/More/Presentation/Pages/more_page.dart';
import 'package:sizer/sizer.dart';

class MainPage extends StatelessWidget {
  MainPage({Key? key}) : super(key: key);
  ChildrenBusLineBloc childrenBusLineBloc = ChildrenBusLineBloc();
  // int initialIndex = 1;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.w),
                  bottomRight: Radius.circular(10.w)),
            ),
            backgroundColor: AppColors.primaryColor,
            title: Padding(
              padding: EdgeInsets.symmetric(vertical: 2.h),
              child: Text(
                "App Driver".tr(context),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 30.sp,
                    fontWeight: FontWeight.bold,
                    color: AppColors.seconedaryColor),
              ),
            ),
            bottom: TabBar(
                padding: EdgeInsets.symmetric(vertical: 1.h, horizontal: 2.w),
                // controller: tabController,
                isScrollable: false,
                labelColor: AppColors.primaryColor,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.w),
                  color: AppColors.whiteColor,
                ),
                splashBorderRadius: BorderRadius.circular(10.w),
                labelStyle:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                // indicatorColor: Colors.transparent,
                unselectedLabelColor: AppColors.whiteColor,
                tabs: [
                  Tab(
                    text: "Children".tr(context),
                  ),
                  Tab(
                    text: "More".tr(context),
                  ),
                ]),
          ),
          body: BackGraoundWidget(
              child: BlocProvider(
            create: (context) => childrenBusLineBloc..add(ChildrenBusLine()),
            child: BlocConsumer<ChildrenBusLineBloc, ChildrenBusLineState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is ErrorToGetChildrenBusLine) {
                  return ErrorMessageWidget(
                      onPressed: () {}, message: state.message);
                }
                if (state is SucccesToGetChildrenBusLine) {
                  return TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    // controller: tabController,
                    children: [
                      ChildrenPage(childrenBusLineBloc: childrenBusLineBloc),
                      const MorePage(),
                    ],
                  );
                } else {
                  return const LoadingWidget();
                }
              },
            ),
          ))),
    );
  }
}
