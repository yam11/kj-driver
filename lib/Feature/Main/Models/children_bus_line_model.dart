class ChildrenBusLineModel {
  Data? data;

  ChildrenBusLineModel({this.data});

  ChildrenBusLineModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  int? lineSupervisorId;
  String? name;
  List<Children>? children;
  LineSupervisor? lineSupervisor;

  Data(
      {this.id,
      this.lineSupervisorId,
      this.name,
      this.children,
      this.lineSupervisor});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    lineSupervisorId = json['lineSupervisorId'];
    name = json['name'];
    if (json['children'] != null) {
      children = <Children>[];
      json['children'].forEach((v) {
        children!.add(new Children.fromJson(v));
      });
    }
    lineSupervisor = json['lineSupervisor'] != null
        ? new LineSupervisor.fromJson(json['lineSupervisor'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['lineSupervisorId'] = this.lineSupervisorId;
    data['name'] = this.name;
    if (this.children != null) {
      data['children'] = this.children!.map((v) => v.toJson()).toList();
    }
    if (this.lineSupervisor != null) {
      data['lineSupervisor'] = this.lineSupervisor!.toJson();
    }
    return data;
  }
}

class Children {
  int? id;
  String? firstName;
  int? guaridanId;
  int? classroomId;
  int? busLineId;
  String? status;
  String? birthDate;

  Children(
      {this.id,
      this.firstName,
      this.guaridanId,
      this.classroomId,
      this.busLineId,
      this.status,
      this.birthDate});

  Children.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    guaridanId = json['guaridanId'];
    classroomId = json['classroomId'];
    busLineId = json['busLineId'];
    status = json['status'];
    birthDate = json['birthDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['guaridanId'] = this.guaridanId;
    data['classroomId'] = this.classroomId;
    data['busLineId'] = this.busLineId;
    data['status'] = this.status;
    data['birthDate'] = this.birthDate;
    return data;
  }
}

class LineSupervisor {
  int? id;
  String? firstName;
  String? lastName;
  int? readyStatus;

  LineSupervisor({this.id, this.firstName, this.lastName, this.readyStatus});

  LineSupervisor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    readyStatus = json['readyStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['readyStatus'] = this.readyStatus;
    return data;
  }
}
