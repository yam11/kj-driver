part of 'my_profile_bloc.dart';

@immutable
abstract class MyProfileEvent {}

class MyProfile extends MyProfileEvent {
  final int id;
  MyProfile({required this.id});
}
