part of 'my_profile_bloc.dart';

@immutable
abstract class MyProfileState {}

class MyProfileInitial extends MyProfileState {}

class SuccessToGetMyProfile extends MyProfileState {}

class ErrorToGetMyProfile extends MyProfileState {
  final String message;
  ErrorToGetMyProfile({required this.message});
}

class LoadingToGetMyProfile extends MyProfileState {}
