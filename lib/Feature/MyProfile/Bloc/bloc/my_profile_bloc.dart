import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjdriver/Core/Api/ExceptionsHandle.dart';
import 'package:kjdriver/Core/Api/Network.dart';
import 'package:kjdriver/Core/Api/Urls.dart';
import 'package:meta/meta.dart';

part 'my_profile_event.dart';
part 'my_profile_state.dart';

class MyProfileBloc extends Bloc<MyProfileEvent, MyProfileState> {
  MyProfileBloc() : super(MyProfileInitial()) {
    on<MyProfileEvent>((event, emit) async{
      if (event is MyProfile) {
        emit(LoadingToGetMyProfile());
        try {
          final response = await Network.getData(
              url: "${Urls.lineSupervisors}${event.id}");
  
          emit(SuccessToGetMyProfile());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetMyProfile(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetMyProfile(message: "error"));
          }
        }
      }
    });
  }
}
