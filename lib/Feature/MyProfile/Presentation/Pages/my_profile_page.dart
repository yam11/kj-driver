import 'package:flutter/material.dart';
import 'package:kjdriver/Core/Constants/app_assets.dart';
import 'package:kjdriver/Core/Constants/app_colors.dart';
import 'package:kjdriver/Core/Widgets/app_bar_widget.dart';
import 'package:kjdriver/Core/Widgets/back_graound_widget.dart';
import 'package:kjdriver/Feature/MyProfile/Presentation/Widgets/Profile_widget.dart';
import 'package:kjdriver/Feature/MyProfile/Presentation/Widgets/more_info_widget.dart';
import 'package:sizer/sizer.dart';

class MyProfilePage extends StatefulWidget {
  const MyProfilePage({Key? key}) : super(key: key);

  @override
  State<MyProfilePage> createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage> {
  bool animated = false;
  @override
  void initState() {
    startAnimation();
    super.initState();
  }

  Future startAnimation() async {
    await Future.delayed(const Duration(milliseconds: 500));
    setState(() {
      animated = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
        child: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 4.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            AnimatedOpacity(
                              duration: const Duration(milliseconds: 350),
                              opacity: animated ? 1 : 0,
                              child: CircleAvatar(
                                radius: 10.w,
                                backgroundColor: AppColors.whiteColor,
                                backgroundImage:
                                    const AssetImage(AppAssets.myProfileIcon),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        AnimatedOpacity(
                          duration: const Duration(milliseconds: 750),
                          opacity: animated ? 1 : 0,
                          child: ProfileWidget(
                            firstName: "Sami",
                            lastName: "Al ahmad",
                            address: "Marquardt Address",
                         
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        AnimatedOpacity(
                          duration: const Duration(milliseconds: 1500),
                          opacity: animated ? 1 : 0,
                          child: MoreInfoWidget(
                            name: "driver@gmail.com",
                            phone: "09876543",
                          ),
                        )
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
